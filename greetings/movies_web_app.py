import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    #db = os.environ.get("RDS_DB_NAME", None)
    #username = os.environ.get("RDS_USERNAME", None)
    #password = os.environ.get("RDS_PASSWORD", None)
    #hostname = os.environ.get("RDS_HOSTNAME", None)
    db = 'testdb'
    username = 'anishbose'
    password = 'anishbose'
    hostname = 'testdb.cdwugbjv4nbd.us-west-2.rds.amazonaws.com'
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    print(db)
    print(username)
    print(password)
    print(hostname)

    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, Title TEXT,Director TEXT, Actor TEXT, Rating DECIMAL (3,2), release_date varchar(30), Year INT, PRIMARY KEY (id))'
    #table_ddl = 'CREATE TABLE movies(id INT UNSIGNED AUTO_INCREMENT, title VARCHAR, lower_title VARCHAR, year INT, director VARCHAR, actor VARCHAR, lower_actor VARCHAR, release VARCHAR, rating FLOAT(2), PRIMARY KEY (id))'
    drop = 'DROP TABLE movies'

    cnx = None
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(drop)
        cnx.commit()
    except Exception as e:
        pass

    try:
        cur.execute(table_ddl)
        cnx.commit()
        #populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")

    title = str(request.form['title'])
    lower_title = title.lower()
    year = int(request.form['year'])
    director = str(request.form['director'])
    actor = str(request.form['actor'])
    lower_actor = actor.lower()
    release_date = str(request.form['release_date'])
    rating = float(request.form['rating'])

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)

    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)

    output = 'Movie ' + title + ' '
    try:
        cur.execute("INSERT INTO movies (Title, Director, Actor, Rating, release_date, Year) values (%s, %s, %s, %s, %s, %s)", (title, director, actor, rating, release_date, year))
        #cur.execute("INSERT INTO movies (title, lower_title, year, director, actor, lower_actor, release, rating) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (title, lower_title, year, director, actor, lower_actor, release, rating))
        cnx.commit()
        output = output + 'successfully inserted'
    except Exception as e:
        output = output + 'could not be inserted - ' + str(e)

    return render_template('index.html', message=output)


@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")

    title = str(request.form['title'])
    lower_title = title.lower()
    year = int(request.form['year'])
    director = str(request.form['director'])
    actor = str(request.form['actor'])
    lower_actor = actor.lower()
    release_date = str(request.form['release_date'])
    rating = float(request.form['rating'])

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)

    output = 'Movie ' + title + ' '
    try:
        cur.execute("UPDATE movies SET Director=%s, Actor=%s, Rating=%s, release_date=%s, Year=%s WHERE lower(Title)=%s",  (director, actor, rating, release_date, year, title.lower(),))
        cnx.commit()
        output = output + 'successfully updated'
    except Exception as e:
        output = output + 'could not be updated - ' + str(e)

    return render_template('index.html', message=output)


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")

    title = str(request.form['delete_title'])
    lower_title = title.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)

    output = 'Movie ' + title + ' '
    try:
        cur.execute("DELETE FROM movies WHERE lower(Title)=%s", (title.lower(),))
        cnx.commit()
        output = output + 'successfully deleted'
    except Exception as e:
        output = output + 'could not be deleted - ' + str(e)

    return render_template('index.html', message=output)


@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request.")

    actor = str(request.args.get('search_actor'))
    lower_actor = actor.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)

    output = ''
    try:
        cur.execute("SELECT Title, Year, Actor FROM movies WHERE lower(Actor)=%s", (actor.lower(),))
        cnx.commit()

        for entry in cur.fetchall():
            ent = str(entry).replace("u'", "'")
            output = output + ent + "\n"
        if output == '':
            output = 'No movies found for actor ' + actor
        

    except Exception as e:
        print(e)
        output = str(e)

    return render_template('index.html', message=output)

@app.route('/highest_rating')
def highest_movie():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    output = ''
    try:
        cur.execute("SELECT Title, Year, Actor, Director, Rating FROM movies WHERE Rating = (SELECT MAX(Rating) from movies)")
        cnx.commit()

        for entry in cur.fetchall():
            ent = str(entry).replace("u'", "'")
            ent = str(ent).replace("Decimal(", "(")
            output = output + ent + "\n"
        if output == '':
            output = 'No movies found'


    except Exception as e:
        print(e)
        output = str(e)

    return render_template('index.html', message=output)

@app.route('/lowest_rating')
def lowest_movie():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    output = ''
    try:
        cur.execute("SELECT Title, Year, Actor, Director, Rating FROM movies WHERE Rating = (SELECT MIN(Rating) from movies)")
        cnx.commit()

        for entry in cur.fetchall():
            ent = str(entry).replace("u'", "'")
            ent = str(ent).replace("Decimal(", "(")
            output = output + ent + "\n"
        if output == '':
            output = 'No movies found'


    except Exception as e:
        print(e)
        output = str(e)

    return render_template('index.html', message=output)


@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    return render_template('index.html')


if __name__ == "__main__":
    print('setting env')
    app.debug = True
    app.run(host='0.0.0.0')
